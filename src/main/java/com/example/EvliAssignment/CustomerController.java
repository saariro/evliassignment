package com.example.EvliAssignment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = "api/Customers")
public class CustomerController {

    @Autowired
    private MajorityOwnerRepository majorityOwnerRepository;
    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody List<MajorityOwner> getCustomers(@RequestParam(value="company", required=true) String companyName, 
    		@RequestParam(value="ownership", defaultValue="25.0") Double ownerShip ) {
    	List<MajorityOwner> owners = majorityOwnerRepository.findOwners(companyName, ownerShip);
        return owners;
    }
}