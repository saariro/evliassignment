package com.example.EvliAssignment;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.Data;

@Data
@Entity
public class Share {
 
    private @Id @GeneratedValue Long id;
    private String company;
    private int number;
    private String ownerSSN;
 
    private Share() {}
 
    public Share(String company, int number, String ownerSSN) {
        this.company = company;
        this.number = number;
        this.ownerSSN = ownerSSN;
    }
}