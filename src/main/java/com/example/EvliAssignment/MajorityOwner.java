package com.example.EvliAssignment;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class MajorityOwner {

    private final String name;
    private final String companyName;
    private final Double ownerShip;
	private final String ssn;
    private final String nationality;
    private final Date birthday;
    private final String taxNumber;

	@Autowired
	JdbcTemplate jdbcTemplate;
    public MajorityOwner(String name, String companyName, Double ownerShip, String ssn, String nationality, Date birthday, String taxNumber) {
        this.name = name;
        this.companyName = companyName;
        this.ownerShip = ownerShip;
        this.ssn = ssn;
        this.nationality = nationality;
        this.birthday = birthday;;
        this.taxNumber = taxNumber;
    }

	public String getTaxNumber() {
		return taxNumber;
	}

	public String getName() {
		return name;
	}

	public String getCompanyName() {
		return companyName;
	}

	public Double getOwnerShip() {
		return ownerShip;
	}
    public String getSsn() {
		return ssn;
	}

	public String getNationality() {
		return nationality;
	}

	public Date getBirthday() {
		return birthday;
	}

}