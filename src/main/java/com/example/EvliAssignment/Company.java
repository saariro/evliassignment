package com.example.EvliAssignment;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.Data;

@Data
@Entity
public class Company {
 
    private @Id @GeneratedValue Long id;
    private @Column(unique=true) String name;
    private @Column(unique=true) String ticker;
    private int shareamount;
 
    private Company() {}
 
    public Company(String name, String ticker, int shareamount) {
        this.name = name;
        this.ticker = ticker;
        this.shareamount = shareamount;
    }
}