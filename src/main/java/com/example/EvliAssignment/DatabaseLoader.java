package com.example.EvliAssignment;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {
 
    private final ClientRepository clientRepository;
    private final CompanyRepository companyRepository;
    private final ShareRepository shareRepository;
 
    @Autowired
    public DatabaseLoader(ClientRepository clientRepository, CompanyRepository companyRepository, ShareRepository shareRepository) {
        this.clientRepository = clientRepository;
        this.companyRepository = companyRepository;
        this.shareRepository = shareRepository;
    }
 
    @Override
    public void run(String... strings) throws Exception {
        this.clientRepository.save(new Client("Warren Buffet", "300830-31i", new Date(30, 7, 30), "US", "1234qwer"));
        this.clientRepository.save(new Client("Carl Icahn", "160236-105t", new Date(36, 1, 16), "US", "dsaddrege"));
        this.clientRepository.save(new Client("George Soros", "120830-099n", new Date(30, 7, 12), "Hu", "sdafafsafd"));
        this.clientRepository.save(new Client("Larry Page", "260373-013n", new Date(73, 2, 26), "US", "sadsdsafafqe3231f"));
        this.clientRepository.save(new Client("Charlie Munger", "010124-093m", new Date(24, 0, 1), "US", "3efefdfdsf"));
        this.companyRepository.save(new Company("Evli", "OTCMKTS:EVLI", 3073));
        this.companyRepository.save(new Company("Nike", "NYSE:NKE", 13200));
        this.companyRepository.save(new Company("Apple", "NASDAQ:AAPL", 5520));
        this.companyRepository.save(new Company("Alphabet Inc", "NASDAQ:GOOGL", 2971));
        for (int i = 1; i < 1000; i++) {
            this.shareRepository.save(new Share("Evli", i, "300830-31i"));
        }
        for (int i = 1001; i < 1500; i++) {
            this.shareRepository.save(new Share("Evli", i, "120830-099n"));
        }
        for (int i = 1500; i < 2200; i++) {
            this.shareRepository.save(new Share("Evli", i, "160236-105t"));
        }
        for (int i = 1; i < 1000; i++) {
            this.shareRepository.save(new Share("Nike", i, "010124-093m"));
        }
        for (int i = 1; i < 1000; i++) {
            this.shareRepository.save(new Share("Apple", i, "120830-099n"));
        }
        for (int i = 1; i < 500; i++) {
            this.shareRepository.save(new Share("Alphabet Inc", i, "120830-099n"));
        }
        for (int i = 501; i < 2000; i++) {
            this.shareRepository.save(new Share("Alphabet Inc", i, "260373-013n"));
        }
    }
}