package com.example.EvliAssignment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class MajorityOwnerRepository
{
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final String SQL_SELECT_ALL_OWNERSHIPS = 
			"SELECT DISTINCT CLIENT.NAME, COMPANY.NAME, COUNT(SHARE.NUMBER), COMPANY.SHAREAMOUNT "
					+ "FROM SHARE "
					+ "JOIN CLIENT ON CLIENT.SSN = SHARE.OWNERSSN "
					+ "JOIN COMPANY ON COMPANY.NAME = SHARE.COMPANY "
					+ "GROUP BY CLIENT.NAME";

	@Transactional(readOnly=true)
	public List<MajorityOwner> findAll() {
		return jdbcTemplate.query(SQL_SELECT_ALL_OWNERSHIPS, 
				new MajorityOwnerRowMapper());
	}

	private static final String SQL_SELECT_MAJORITY_CLIENTS = 
			"SELECT DISTINCT CLIENT.NAME, COMPANY.NAME, COUNT(SHARE.NUMBER), COMPANY.SHAREAMOUNT, CLIENT.SSN, CLIENT.BIRTHDAY, CLIENT.NATIONALITY, CLIENT.TAXNUMBER "
					+ "FROM SHARE "
					+ "JOIN CLIENT ON CLIENT.SSN = SHARE.OWNERSSN "
					+ "JOIN COMPANY ON COMPANY.NAME = SHARE.COMPANY "
					+ "WHERE SHARE.COMPANY = ? "
					+ "GROUP BY CLIENT.NAME "
					+ "HAVING (COUNT(SHARE.NUMBER) * 100 / COMPANY.SHAREAMOUNT) > ?";

	@Transactional(readOnly=true)
	public List<MajorityOwner> findOwners(String company, Double ownership) {
		return jdbcTemplate.query(SQL_SELECT_MAJORITY_CLIENTS, new Object[] { company, ownership },
				new MajorityOwnerRowMapper());
	}

}

class MajorityOwnerRowMapper implements RowMapper<MajorityOwner> {


	@Override
	public MajorityOwner mapRow(ResultSet rs, int row) throws SQLException {
		return new MajorityOwner(rs.getString(1), rs.getString(2), rs.getDouble(3) / rs.getDouble(4), rs.getString(5), rs.getString(7), rs.getDate(6), rs.getString(8));
	}
}