package com.example.EvliAssignment;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.Data;

@Data
@Entity
public class Client {
 
    private @Id @GeneratedValue Long id;
    private String name;
    private @Column(unique=true) @Size(max = 11) String ssn;
    private Date birthday;
    private @Size(max = 2) String nationality;
    private @Column(unique=true) @Size(max = 40)String taxnumber;
 
    private Client() {}
 
    public Client(String name, String ssn, Date birthday, String nationality, String taxnumber) {
        this.name = name;
        this.ssn = ssn;
        this.birthday = birthday;
        this.nationality = nationality;
        this.taxnumber = taxnumber;
    }
}
