package com.example.EvliAssignment;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ShareRepository extends PagingAndSortingRepository<Share, Long> {
	
	List<Share> findByCompany(@Param("company") String company, Pageable pageRequest);
}
