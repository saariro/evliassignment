'use strict';

// tag::vars[]
const React = require('react');
const ReactDOM = require('react-dom')
const client = require('./client');
const bootstrap = require('react-bootstrap');
// end::vars[]

// tag::app[]
class App extends React.Component {
 	constructor(props) {
		super(props);
		this.state = {tableData: [], view: 1, filterText: ''};
		this.changeView = this.changeView.bind(this);
		this.handleFilterTextInput = this.handleFilterTextInput.bind(this);
	}


  changeView(view, company) {
	if (view == 1) {
		client({
			method: 'GET',
			path: 'http://localhost:8080/api/companies',
			headers: {'Accept': 'application/schema+json'}
		}).then(data => {
		  this.setState({tableData: data.entity.content, view: view });
		});
	}
	if (view == 2) {
		 client({
			method: 'GET',
			path: 'http://localhost:8080/api/shares/search/findByCompany?company=' + company,
			headers: {'Accept': 'application/schema+json'}
		}).then(data => {
			this.setState({
			  view: view,
			  tableData: data.entity.content
			})			
		});
		}	
	if (view == 3) {
		 client({
			method: 'GET',
			path: 'http://localhost:8080/api/Customers?company=' + company + '&ownership=5.0',
			headers: {'Accept': 'application/schema+json'}
		}).then(data => {
			this.setState({
			  view: view,
			  tableData: data.entity
			})			
		});
		}
  }
  
  
  handleFilterTextInput(filterText) {
    this.setState({
      filterText: filterText
    });
  }
  
  componentDidMount() {
    this.changeView(1, null);
  }
 
  render() {
return ( 
	<div className="container">
		<Header filterText={this.state.filterText} onFilterTextInput={this.handleFilterTextInput} changeView = {this.changeView} view={this.state.view}/>
		<div className="row">
			<DataTable tableData={this.state.tableData} filterText={this.state.filterText} view={this.state.view} changeView = {this.changeView}/>
		</div>			
	</div>
	)
  }
}
// end::app[]


// tag::data-table[]
class DataTable extends React.Component {
 	constructor(props) {
		super(props);
		this.changeView = this.changeView.bind(this)
	}
 
	changeView(view, company) {
		this.props.changeView(view, company);

	}

  render() {
    if (this.props.view==1) {
    var companies = this.props.tableData.map(company =>
		<Company key={company.links[0].href} company={company} view={this.props.view} changeView = {this.changeView} />);
    return (
		<div className="container">
		  <table className="table table-striped">
		    <thead>
				<tr>
		            <th>Name</th><th>Ticker</th><th>Shares</th>
				</tr>
		      </thead>
		    <tbody>{companies}</tbody>
		  </table>
		</div>
		)
	}
    if (this.props.view==2) {
		if (this.props.tableData) {
			var shares = this.props.tableData.map(share =>
				<Share key={share.links[0].href} share={share} view={this.props.view} changeView = {this.changeView} />);
    return (
		<div className="container">
		  <table className="table table-striped">
		    <thead>
				<tr>
		            <th>Company</th><th>Share number</th><th>Owner</th>
				</tr>
		      </thead>
		    <tbody>{shares}</tbody>
		  </table>
		</div>
		)
	}
	else {
		return null;
	}
	}
    if (this.props.view==3) {
    var owners = this.props.tableData.map(owner =>
		<Owner owner={owner} filterText={this.props.filterText} view={this.props.view} changeView = {this.changeView} />);
    return (
		<div className="container">

			<div className="row">
			  <table className="table table-striped">
				<thead>
					<tr>
						<th>Company</th><th>Owner name</th><th>SSN</th><th>Birthday</th><th>Nationality</th><th>Tax number</th><th>Ownership</th>
					</tr>
				  </thead>
				<tbody>{owners}</tbody>
			  </table>
			</div>
		</div>
		)
	}
  }
}
// end::data-table[]

// tag::company[]
class Company extends React.Component {
  constructor(props) {
    super(props);
    this.getShares = this.getShares.bind(this);
    this.getOwners = this.getOwners.bind(this);
	this.changeView = this.changeView.bind(this)
  }
	changeView(view, company) {
		this.props.changeView(view, company);
	}

  getOwners(e) {
	e.preventDefault();
        this.changeView(3, this.props.company.name);
  }
  getShares(e) {
	e.preventDefault();
        this.changeView(2, this.props.company.name);
  }
  render() {
    return (
		<tr>
	        <td>{this.props.company.name}</td>
	        <td>{this.props.company.ticker}</td>
	        <td>{this.props.company.shareamount}</td>
			<td>
			    <button className="btn btn-info" onClick={this.getShares}>Shares</button>
			</td>
			<td>
			    <button className="btn btn-info" onClick={this.getOwners}>Owners</button>
			</td>
		</tr>
		)
  }
}
// end::company[]

// tag::share[]
class Share extends React.Component {
  constructor(props) {
    super(props);
	this.changeView = this.changeView.bind(this)
  }
	changeView(view, company) {
		this.props.changeView(view, company);
	}
	
  modify(e) {
	e.preventDefault();
        this.changeView(2, this.props.company.name);
  }
  render() {
    return (
		<tr>
	        <td>{this.props.share.company}</td>
	        <td>{this.props.share.number}</td>
	        <td>{this.props.share.ownerSSN}</td>
		</tr>
		)
  }
}
// end::share[]



// tag::owner[]
class Owner extends React.Component {
  constructor(props) {
    super(props);
	this.changeView = this.changeView.bind(this)
  }
	changeView(view, company) {
		this.props.changeView(view, company);
	}
	
  render() {
	if (Math.round(this.props.owner.ownerShip * 100*100)/100 < parseInt(this.props.filterText)) {
		return null;
	}
    return (
		<tr>
	        <td>{this.props.owner.companyName}</td>
	        <td>{this.props.owner.name}</td>
	        <td>{this.props.owner.ssn}</td>
	        <td>{this.props.owner.birthday}</td>
	        <td>{this.props.owner.nationality}</td>
	        <td>{this.props.owner.taxNumber}</td>
	        <td>{Math.round(this.props.owner.ownerShip * 100*100)/100} %</td>
		</tr>
		)
  }
}
// end::owner[]

// tag::header[]
class Header extends React.Component {
  constructor(props) {
    super(props);
	this.changeView = this.changeView.bind(this)
    this.handleFilterTextInputChange = this.handleFilterTextInputChange.bind(this);
  }
	changeView(view, company) {
		this.props.changeView(view, company);
	}
  handleFilterTextInputChange(value) {
    this.props.onFilterTextInput(value);
  }
  render() {
	return (
	
	<div className="container">
		<div className="page-header">
			<h1>Evli Stock Assignment</h1>
		</div>
		<div className="row">
			<div className="col-lg-3">
				<button type="button" className="btn btn-info" onClick={() => this.changeView(1, null)} disabled={this.props.view == 1}>
					<span className="glyphicon glyphicon-arrow-left"></span> Previous
				</button>
			</div>
			<div className="col-lg-3 text-center">
				{ this.props.view == 3 ? <SearchBar filterText={this.props.filterText} onFilterTextInput={this.handleFilterTextInputChange} view={this.props.view}/> : null }
			</div>
		</div>
		</div>
    );
  }
}
// end::header[]

// tag::searchbar[]
class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.handleFilterTextInputChange = this.handleFilterTextInputChange.bind(this);
  }
  
  handleFilterTextInputChange(e) {
    this.props.onFilterTextInput(e.target.value);
  }
  
  render() {
    return (
      <form>
        <input className="form-control" type="text" placeholder="Filter minimum ownership" value={this.props.filterText} onChange={this.handleFilterTextInputChange} />
      </form>
    );
  }
}
// end::searchbar[]


// tag::render[]
ReactDOM.render(<App />, document.getElementById('root') );
// end::render[]